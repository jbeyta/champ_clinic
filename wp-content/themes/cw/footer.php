<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

	<footer role="contentinfo">
		<div class="row">
			<div class="large-4 columns left">
				<h4>Call or Come By</h4>
				<p><strong>940-322-2400</strong></p>
				<p>2934 Kemp Blvd.</p>
				<p>Wichita Falls, Tx 76308</p>

				<h4>Hours:</h4>
				<p>7:30am-5pm, Mon. - Thurs.</p>
				<p>7:30am-Noon Fri.</p>
				<p>Closed, Sat. &amp; Sun.
			</div>

			<div class="large-4 columns">
				<p>&copy; <?php echo date('Y'); ?> Champions's Clinic</p>
				<p><a href="/privacy-policy">Privacy Policy</a> | <a href="/terms-of-use">Terms and Conditions</a>
				<p class="logo large"><img src="<?php echo get_template_directory_uri(); ?>/images/crane_logo.png"></p>
			</div>

			<div class="large-4 columns widget contact">
				<h4>Request an Appointment</h4>

				<?php gravity_form(2, false, false, false, '', true, 12); ?>
				<p class="logo hide small"><img src="<?php echo get_template_directory_uri(); ?>/images/crane_logo.png"></p>
			</div>
		</div>
	</footer>

	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.0.3.min.js"><\/script>')</script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/production.js"></script>
</body>
</html>