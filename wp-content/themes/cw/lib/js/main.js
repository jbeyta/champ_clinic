jQuery(document).ready(function($){
	var imgContainer = $('.img-contain');
	var slide = imgContainer.find('.slide');
	var caption = imgContainer.find('.content');
	var more = imgContainer.find('#more');
	var menu = $('.slideshow .menu .link');
	var current = 0;

	$('.slideshow .menu li a').click(function(e){
		var img = $(this).data('img');
		var content = $(this).data('caption');
		var link = $(this).data('link');

		$('.slideshow .menu .current').removeClass('current');
		$(this).closest('.link').addClass('current');
		setCurrent( link );

		slide.fadeOut('fast', function() {
			slide.attr('src', img).fadeIn('fast');
		});

		caption.fadeOut('fast', function(){
			caption.html(content).fadeIn('fast');
		});

		more.fadeOut('fast', function(){
			if( link.length ) {
				more.attr('href', link).fadeIn('fast');
			} 
		});

		resetSlide();

		return false;
	});

	function setCurrent(link) {
		for( var i = 0; i < menu.length; i++ ) {
			var temp = $(menu[i]).find('a');
			var tempLink = temp.data('link');

			if ( tempLink == link ) {
				current = i;
				break;
			}
		}
	}

	// Pause slideshow on hover
	$('.slideshow').hover( function(){
		clearInterval(intv);
	}, function(){
		startSlide();
	});

	function resetSlide() {
		clearInterval(intv);
		startSlide();
	}

	function startSlide(){
		intv = setInterval(function() {
			if( typeof menu[current+1] !== 'undefined' )
				temp = current + 1;
			else
				temp = 0;

			$(menu[temp]).find('a').click();

		}, 5000 );       
	}
	startSlide();

	$('.fitvids').fitVids();

	$('.fancyDropdown').change(function(){
		var url = $(this).val();
		if ( url.length ) {
			window.location.href = url;
		}
	});

	var footerFields = $('[role="contentinfo"] .gform_body .gfield');

	for( var i = 0; i < footerFields.length; i++ ) {
		var temp = $(footerFields[i]);

		var label = temp.find('label');
		var input = temp.find('input,textarea');

		input.attr('placeholder', label.text());
	}
});