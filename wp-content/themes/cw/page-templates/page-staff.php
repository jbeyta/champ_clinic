<?php
/**
 * Template Name: Staff Page Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div role="main" class="staff row">
		<h2 class="page-header"><?php the_title(); ?></h2>
		<div id="mason" class="large-8 push-4 columns">
			<?php $post_type = 'staff'; ?>
			<!-- get categories -->
			<?php $taxonomies = get_object_taxonomies( (object) array( 'post_type' => staff) ); ?>
			<?php foreach( $taxonomies as $taxonomy ) : ?>
				<!-- get category terms -->
				<?php $terms = get_terms( $taxonomy ); ?>
				<?php foreach( $terms as $term ) : ?>
					<!-- display the terms -->
					<h2 class="section-header"><?php echo $term->name ?></h2>
					<div class="listings-container">	
					<!-- get the posts -->
					<?php $posts = new WP_Query( "taxonomy=$taxonomy&term=$term->slug&posts_per_page=-1&orderby=title&order=ASC" ); ?>
					<?php if( $posts->have_posts() ): ?>
						<!-- list em out -->
						<?php while( $posts->have_posts() ) : $posts->the_post(); ?>
							<div class="listing large-6 columns">
								<div class="inner">

									<!-- check for image -->
									<?php if( has_post_thumbnail() ) { ?><div class="listing-image"><?php the_post_thumbnail('staff-thumb') ?></div><?php }?>
									<div class="listing-info<?php if( has_post_thumbnail() ) { ?> has-image<?php }?>">
										<!-- all of the info, if it exists -->
										<?php $content = get_the_content(); if( !empty($content) ) { ?>
										<a href="<?php the_permalink()?>"><h3><?php the_title() ?></h3></a>
										<?php } else { ?>
										<h3><?php the_title() ?></h3>
										<?php } ?>
										<p><?php $grade = get_post_meta($post->ID, "_grade", true); if ( !empty( $grade ) ) { ?><?php echo $grade ?><?php } ?></p>
										<p><?php $phone = get_post_meta($post->ID, "_phone", true); if ( !empty( $phone ) ) { ?><?php echo $phone ?><?php } ?></p>
										<?php $email = get_post_meta($post->ID, "_email", true); if ( !empty( $email ) ) { ?><a href="mailto:<?php echo $email ?>">Email</a><?php } ?>
										<?php $link = get_post_meta($post->ID, "_link", true); if ( !empty( $link ) ) { ?><a href="mailto:<?php echo $link ?>"><?php echo $link ?></a><?php } ?>

									</div>
								</div>
							</div>
						<?php endwhile; // end of the loop. ?>
					<?php endif; ?>
					</div>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>