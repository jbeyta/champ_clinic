<?php
/**
 * Template Name: FAQs Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<?php
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$args = array(
			'post_type' => 'faqs',
			'posts_per_page' => 4,
			'orderby'=> 'date',
			'order' => 'DESC',
			'paged' => $paged
		);
		query_posts($args);
	?>

	<div class="main row" role="main">
		<div class="large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<article class="cf">
					<h3 class="entry-title"><?php the_title(); ?></h3>
					<?php if( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>

			<div class="navigation cf">
				<div class="paging left"><?php previous_posts_link('Previous') ?></div>
				<div class="paging right"><?php next_posts_link('Next') ?></div>
			</div>
			<?php wp_reset_query(); ?>
		</div>

		<div class="large-4 columns">
			<?php get_sidebar(); ?>
		</div>

	</div>

<?php get_footer(); ?>