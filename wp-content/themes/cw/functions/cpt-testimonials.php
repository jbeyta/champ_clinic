<?php
/**
 * Slideshow Custom Post Type
 *
 * @since CW 1.0
 */

function cw_cpp_testimonials_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Testimonials' ),
			'singular_name' => __( 'Testimonial' ),
			'add_new' => __( 'Add New Testimonial' ),
			'add_new_item' => __( 'Add New Testimonial' ),
			'edit_item' => __( 'Edit Testimonial' ),
			'new_item' => __( 'Add New Testimonial' ),
			'view_item' => __( 'View Testimonial' ),
			'search_items' => __( 'Search Testimonials' ),
			'not_found' => __( 'No testimonials found' ),
			'not_found_in_trash' => __( 'No testimonials found in trash' )
		),
		'public' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'menu_position' => 20,
		'supports' => array('title')
	);
	register_post_type('testimonial',$field_args);
}
add_action( 'init', 'cw_cpp_testimonials_init' );

/**
 * Remove WYSIWYG Editor from testimonials
 *
 * @since CW 1.0
 */

function cw_cpp_testimonials_remove_wysiwyg() {
    global $pagenow;
    if ( 'post.php' == $pagenow ) {
        $type = get_post_type( $_GET['post'] );
        if( $type == 'testimonial' ) {
          add_filter('user_can_richedit', '__return_false');
          remove_action( 'media_buttons', 'media_buttons' );
        }

    } elseif ( 'post-new.php' == $pagenow ) {
        if( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'testimonial') {
			add_filter('user_can_richedit', '__return_false');
			remove_action( 'media_buttons', 'media_buttons' );
        }
    }
}
add_action( 'admin_init', 'cw_cpp_testimonials_remove_wysiwyg' );

/**
 * Testimonials Custom Fields
 *
 * @since CW 1.0
 */
function cw_cpp_testimonials_metaboxes() {
	add_meta_box('testimonial', 'Testimonial', 'cw_cpp_testimonial', 'testimonial', 'normal', 'high');
	add_meta_box('testimonials_meta', 'Testimonial Meta', 'cw_cpp_testimonials_meta', 'testimonial', 'normal', 'default');
}
add_action( 'add_meta_boxes', 'cw_cpp_testimonials_metaboxes' );

function cw_cpp_testimonial() {
	global $post;

	echo '<textarea class="widefat" name="testimonial" rows="16">'.get_post_meta($post->ID, 'testimonial', true).'</textarea>';
}

function cw_cpp_testimonials_meta() {
	global $post;
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="testimonial_meta_noncename" id="testimonial_meta_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the data if there is any.
	$meta = get_post_meta($post->ID, 'testimonial_meta', true);

	// Echo out the field
	echo '<input type="text" name="testimonial_meta" class="widefat" value="'.$meta.'">';
}

function cw_cpp_testimonials_save_meta($post_id, $post) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['testimonial_meta_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	$meta['testimonial_meta'] = $_POST['testimonial_meta'];
	$meta['testimonial'] = $_POST['testimonial'];

	// Add values of $meta as custom fields
	foreach ($meta as $key => $value) { // Cycle through the $meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}
}
add_action('save_post', 'cw_cpp_testimonials_save_meta', 1, 2); // save the custom fields

?>