module.exports = function(grunt) {

  grunt.initConfig({

	pkg: grunt.file.readJSON('package.json'),

	compass: {
		dist: {
			options: {
				sassDir: 'lib/scss',
				cssDir: 'css'
			}
		}
	},

	cssmin: {
		css:{
			src: 'css/style.css',
			dest: 'css/style.min.css'
		}
	},

	jshint: {
	  beforeconcat: ['lib/js/*.js']
	},

	concat: {
	  dist: {
		src: [
		  'lib/js/plugins/*',
		  'lib/js/foundation.min.js',
		  'lib/js/main.js',
		],
		dest: 'js/production.js'
	  }
	},

	uglify: {
	  build: {
		src: 'js/production.js',
		dest: 'js/production.min.js'
	  }
	},

	imagemin: {
	  dynamic: {
		files: [{
		  expand: true,
		  cwd: 'images/',
		  src: ['**/*.{png,jpg,gif}'],
		  dest: 'images/'
		}]
	  }
	},

	watch: {
	  options: {
		livereload: true,
	  },
	  scripts: {
		files: ['lib/js/*.js'],
		tasks: ['concat', 'uglify'],
		options: {
		  spawn: false,
		}
	  },
	  css: {
		files: ['lib/scss/*.scss'],
		tasks: ['compass', 'cssmin'],
		options: {
		  spawn: false,
		}
	  },
	},

	connect: {
	  server: {
		options: {
		  port: 8000,
		  base: './'
		}
	  }
	},

  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);

};