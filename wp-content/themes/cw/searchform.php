<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<input type="search" name="s" id="s" placeholder="Search...">
	</form>