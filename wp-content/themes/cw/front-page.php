<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */

get_header(); ?>

	<div class="main row" role="main">
		<div class="large-12 columns slideshow">
			<div class="row">
				<div class="large-4 small-12 columns menu">
					<ul>
						<li class="none"><h5>Physical Medicine and Rehab</h5></li>
						<?php
							$slides = new WP_Query( array('post_type' => 'slideshow', 'orderby' => 'menu_order', 'order' => 'asc') );
							if ( $slides->have_posts() ) {
								$i = 1;
								
								while ( $slides->have_posts() ) {
									$current = '';
									$slides->the_post();

									if( $i == 1 ) {
										$current = 'current';
									}

									echo '<li class="link '.$current.'"><a data-link="'.get_post_meta($post->ID, '_slideshowlink', true).'" data-img="'.wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ).'" data-caption="'.htmlspecialchars( wpautop( get_the_content() ) ).'">'.get_the_title().'</a></li>';
									$i++;
								}

							}
						?>
					</ul>
				</div>
				<div class="large-8 hide-for-small hide-for-medium columns img-contain slides">
					<?php
						$slides->rewind_posts();
						if ( $slides->have_posts() ) {
							global $post;
							$slides->the_post();
							?>
							<img class="slide" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );?>">
							<div class="caption">
								<div class="content">
									<?php the_content();?>
								</div>
								<a href="<?php echo get_post_meta( $post->ID, '_slideshowlink', true ); ?>" class="button" id="more">Learn More</a>
							</div>
						<?php
						}
					?>
				</div>
			</div>
		</div>


		<div class="large-8 columns content left">
			<div class="row">
				<div class="large-6 columns">
					<h3>First Visit?</h3>
					<p>From your first visit to your last, we explain everything you'll need to know...</p>
					<a href="/what-to-expect" class="button">What can I expect?</a>
				</div>

				<div class="large-6 columns pediatric-care">
					<h3>Pediatric Care</h3>
					<p>is more important than you think. Starting children early is good.</p>
					<a href="/pediatric-care" class="button">Learn More</a>
				</div>
			</div>

			<div class="row section-shadow">
				<div class="large-12 columns meet-docs">
					<h3>Meet the Doctors</h3>

					<img style="display: block; margin: 0 auto;" class="home-docs aligncenter" src="<?php echo get_template_directory_uri(); ?>/images/docs_2.png">

					<p>In traditional medicine the focus is to treat the disease once it occurs. At Champions Clinic the focus is on improving your health to reduce the risk of pain and illness. Most would rather be healthy and avoid illness instead of waiting to treat whatever sickness occurs. This is a large part of why there’s been a large surge of interest in Champions Clinic.</p>

					<p>People are recognizing the benefit of seeking an alternative to traditional medicine. They understand that it will help them achieve and maintain optimal health. Take the time to meet the doctor, team, and wellness network that are all dedicated to helping you achieve your wellness objectives.</p>

					<a href="/about-us/" class="button">More About Us</a>
				</div>
			</div>
		</div>

		<aside role="complementary" class="large-4 columns sidebar">
			<?php get_sidebar(); ?>
		</aside>
		</div>
	</div>

	<div class="row section-shadow">
		<div class="large-6 columns testimonials left">
			<h3>Testimonials</h3>
			<?php
				$testimonials = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => 1, 'orderby' => 'rand' ) );

				if( $testimonials->have_posts() ) {
					while( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
						<blockquote class="testimonial">
							<?php echo wp_trim_words( get_post_meta($post->ID, 'testimonial', true) , 20, '...'); ?>				
						</blockquote>

						<p>- <?php the_title(); ?></p>
					<?php endwhile;
				}
			?>

			
		</div>

		<div class="large-6 columns right fitvids">
			<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/video.png"> -->
			<?php
				if ( false === ( $videos = get_transient( 'champions_youtube_videos' ) ) ) {
					// It wasn't there, so regenerate the data and save the transient
				

					$videos = json_decode(
						wp_remote_retrieve_body(
							wp_remote_get('https://gdata.youtube.com/feeds/api/videos?q=champions%20clinic%20testimonial&max-re%20%E2%80%8Bsults=5&v=2&alt=jsonc&orderby=published')
						)
					);

					$videos = $videos->data->items;
					$keeps = array();
					
					foreach( $videos as $key => $video ) {
						if( trim($video->uploader) == 'avxxA8yUA1fb6ltk1SBwPQ' )
							$keeps[] = $video->id;
					}

					$videos = $keeps;
					set_transient( 'champions_youtube_videos', $videos, 600 );
				}
				// Used to randomly choose a video
				$randomKey = array_rand($videos); ?>

				<iframe width="420" height="315" src="//www.youtube.com/embed/<?php echo $videos[$randomKey];?>?showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
		</div>

	</div><!-- .content -->
<?php get_footer(); ?>