<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Facebook Like Button SDK-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561068250587379";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<nav role="navigation">
		<div class="row">
			<div class="small-12 medium-12 large-3 columns contact cf">
				<a href="tel:9403222400">940-322-2400</a> | <div class="fb-like contact" data-href="http://developers.facebook.com/docs/reference/plugins/like" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
			</div>

			<div class="small-12 medium-8 large-9 columns nav hide-for-small hide-for-medium">
				<div class="portal">
					<!-- <a href="#">New Patient Center</a> <br><span>What to bring & what to expect.</span> -->
				</div>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu inline-list cf', 'container_class' => 'menu-main-container cf' ) ); ?>
			</div>
		</div>
	</nav>
	<header role="banner" class="row">
		<div class="medium-9 large-9 columns cf">
			<a href="/"><h1 class="hide-text"><?php bloginfo( 'name' ); ?></h1></a>
			<p class="tagline">Feel Like a Winner!</p>
		</div>
		<div class="medium-3 large-3 columns vert-input">
			<?php get_search_form( true ); ?>
		</div>
		<div class="large-12 columns hide-for-large-up">
				<?php
					$locations = get_nav_menu_locations();
					$menu = wp_get_nav_menu_object( $locations[ 'primary' ] );
					$items = wp_get_nav_menu_items($menu->term_id);
					echo '<form class="custom"><select class="fancyDropdown">';
					echo '<option value="">Menu</option>';
					foreach($items as $key => $item) {
						echo '<option value="'.$item->url.'">'.$item->title.'</option>';
					}
					echo '</select></custom>';
					echo '</form>';
				?>
			</div>
	</header>