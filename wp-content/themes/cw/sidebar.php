<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

	<aside class="widget-area" role="complementary">
		<div class="row widget">
			<h3>Stay Informed</h3>

			<p>We offer some information that you can't get anywhere else. All that we ask is that you sign up for our monthly newsletter and we give you access to a wealth of wellness information. It's absolutely FREE!</p>

			<form action="http://championsclinic.us3.list-manage2.com/subscribe/post?u=6db823321123be557dda84190&amp;id=39cd4b061d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<input type="email" name="EMAIL" placeholder="Enter Your Email for Great Deals..." required>
				<button class="button">Sign Up!</button>
			</form>
		</div>

		<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar-1')) : endif;?>
	</aside>