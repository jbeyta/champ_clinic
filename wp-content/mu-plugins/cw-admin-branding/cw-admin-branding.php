<?php

/*
Plugin Name: Crane|West Admin Branding
Plugin URI: http://ericbinnion.com
Description: Provides simple branding of WordPress admin for Crane|West client websites. Simply overwrite the images with client logos and replace powered by text to something appropriate.
Version: 0.001
Author: Eric Binnion
Author URI: http://www.ericbinnion.com
License: GPLv2
*/

/**
 * @desc Adds favicon to WordPress admin
 */

// function cw_admin_add_fav(){

// 	$favicon_test = ABSPATH. 'http://cdn.onextrapixel.com/favicon.ico'; //use correct path to favicon.ico here
// 	if(!file_exists($favicon_test))
// 		return;

// 	$favicon = home_url('favicon.ico'); //use correct url to favicon.ico here
// 	echo "<link href='{$favicon}' rel='shortcut icon' type='image/x-icon' >";
// }
// add_action('admin_enqueue_scripts', 'cw_admin_add_fav', 1);


/**
 * @desc Removes WordPress admin logo/menu
 */

function cw_admin_edit_wp_admin_logo() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->add_menu(
		array(
			'title' => '<img src="'. plugins_url( 'images/cw_logo.png', __FILE__ ) .'" style="vertical-align:middle;margin-right:5px; max-width: 28px; max-height: 60%; height: auto; width: auto;" alt="Visit Site" title="Visit Site" />Crane|West',
			'id' => 'crane-west-menu',
			'href' => false,
			'parent' => false,
			'meta' => array(
				'class' => 'developer-admin-logo'
			)
		)
	);

	$wp_admin_bar->add_menu(
		array(
			'title' => 'Crane|West Website',
			'id' => false,
			'href' => 'http://crane-west.com',
			'parent' => 'crane-west-menu',
			'meta' => array(
				'target' => '_blank'
			)
		)
	);

	$wp_admin_bar->add_menu(
		array(
			'title' => 'Contact Support',
			'id' => false,
			'href' => 'mailto:support@crane-west.com',
			'parent' => 'crane-west-menu',
			'meta' => array(
				'target' => '_blank'
			)
		)
	);
}
add_action( 'wp_before_admin_bar_render', 'cw_admin_edit_wp_admin_logo');

/*
 * @desc Edits WordPress login page
 */
function cw_admin__wp_login_url() {
    return get_option('home');
}

function cw_admin_wp_login_title() {
    return get_option('blogname') .' - Powered by Crane|West';
}

function cw_admin_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background: url(<?php echo plugins_url( 'images/login_logo.png', __FILE__ ) ?>) no-repeat center center;
			-webkit-background-size: contain;
			-moz-background-size: contain;
			background-size: contain;
            padding-bottom: 30px;
        }
    </style>
<?php }

add_action('login_enqueue_scripts', 'cw_admin_login_logo' );
add_filter('login_headerurl', 'cw_admin__wp_login_url');
add_filter('login_headertitle', 'cw_admin_wp_login_title');

/**
 * @desc Replaces WordPress logo in admin nav bar.
 */
function cw_admin_custom_admin_css() { ?>
	<style>
		.developer-admin-logo {
			background: url(<?php echo plugins_url( 'images/cw_logo.png', __FILE__ ) ?>) no-repeat left center;
		}
	</style>
<?php
}

add_action('admin_head','cw_admin_custom_admin_css');

/*
 * @desc Replaces WordPress footer text
 */
function cw_admin_filter_footer_admin() { ?>
Built with love by <a href="http://crane-west.com">Crane|West</a> |  For support, please <a href="mailto:support@crane-west.com" target="_blank">email</a> or call us at 940-691-2111
<?php }

add_filter('admin_footer_text', 'cw_admin_filter_footer_admin');
/**END REPLACE FOOTER TEXT**/